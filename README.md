# Übungsblätter thematisch ergänzen:
    - Compile Error in Ü1 Erste Ableitung bestimmen
    - Übungsblatt 2 Aufgabe 5
    - Übungsblatt 1 Aufgabe 7 Bild einfügen

# Formataufgaben:
    - Python Dateien in eigene Dateien exportieren
    - YAML-Header von Luca übernehmen  
    - Graphen richtig skalieren

# Sonstige Aufgaben:
    - Einzelaufgaben hochladen 

# Punkte von Luca:
    - Gedanken machen über Integration mit Python 
    - In den Teams zusammensetzen, um Inhalte zu besprechen 
        - Was die Studenten lernen sollen 
        - Inhaltliche Tiefe der Aufgaben machen
    - VL im PDF-Editor angucken und kommentieren 
    - Welches Ausmaß soll Python haben ?