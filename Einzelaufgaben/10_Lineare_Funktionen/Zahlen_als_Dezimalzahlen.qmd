---
title: Aufgabe 1.7
---
:::{.content-visible unless-profile="lösungen"}
Schreiben Sie die folgenden Zahlen als einfache Dezimalzahlen:

$a) \qquad 3-\frac{19}{7}$\
$b) \qquad \frac{45,6}{0,02}$\
$c) \qquad 0,05*20$\
$d) \qquad \left(\frac{16 \cdot 10^4}{2 \cdot 5000}\right)^{0,5}$\
$e) \qquad 0,64^{-0,5}$
:::

:::{.content-hidden when-profile="lösungen"}
a) $3-\dfrac{19}{7}=\dfrac{21}{7}-\dfrac{19}{7}=\dfrac{2}{7}=0.2857142857142857...=0,\overline{285714}$

b) $\dfrac{45,6}{0,02}=\dfrac{4516}{\dfrac{2}{100}}=\dfrac{45.6\cdot 100}{2}=\dfrac{4560}{2}=2280$

c) $0,05\cdot20=1$

d) $\left( \dfrac{16\cdot 10^{4}}{2\cdot 5000}\right) ^{015}=16^{0.5}=\sqrt{16}=4$

e) $0,64^{-0.5}=\dfrac{1}{0,64^{015}}=\dfrac{1}{\sqrt{0,64}}=\dfrac{1}{0,8}=\dfrac{10}{8}=1,25$
:::
