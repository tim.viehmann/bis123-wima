---
title: Grenzwert_angeben_falls_existiert
format: pdf
---
:::{.content-visible unless-profile="lösungen"}
Geben sie den Grenzwert an, falls dieser existiert (ggf. auch $\pm \infty$ ):
a) $\lim _{x\to\infty }\dfrac{1}{x^2}\cdot \sin{(x)}$
b) $\lim _{x\to-\infty }\dfrac{1}{x^2}\cdot \sin{(x)}$
c) $\lim _{x\to\infty }\dfrac{-3x+2}{4x-5}$
d) $\lim _{x\to\infty }2^{-2x}$
e) $\lim _{x\to-\infty }2^{-2x}$
f) $\lim _{x\to-\infty }\dfrac{x^2}{x+1}$

Geben sie jeweils eine kurze Begründung (z.B. durch eine Rechnung bzw. Umformung) an. 
:::

:::{.content-visible when-profile="lösungen"}
a) $\displaystyle \lim_{x\to \infty}\frac{1}{x^2} \cdot \sin(x)=\frac{1}{''\infty''} \cdot \sin(''\infty'')=0 \cdot \sin(''\infty'')=0$

b) $\displaystyle \lim_{x\to -\infty}\frac{1}{x^2} \cdot \sin(x)=\frac{1}{''\infty''} \cdot \sin(''-\infty'')=0 \cdot \sin(''-\infty'')=0$

c) $\displaystyle\lim_{x\to \infty}\frac{-3x+2}{4x-5}=\displaystyle\lim_{x\to \infty} \, \frac{-3}{4}=-0,75$

d) $\displaystyle \lim_{x\to \infty}2^{-2x}=\displaystyle \lim_{x\to \infty} \, \frac{1}{2^{2x}}=\frac{1}{''\infty''}=0$

e) $\displaystyle \lim_{x\to -\infty}2^{-2x}=\displaystyle \lim_{x\to \infty} \, 2^{2x}=\infty$ 

f) $\displaystyle \lim_{x\to -\infty}\frac{x^2}{x+1}=\displaystyle \lim_{x\to -\infty}\frac{x}{1}=-\infty$
:::