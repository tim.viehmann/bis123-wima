---
title: Grenzwerte_bestimmen_mit_Begründung
format: pdf
---
:::{.content-visible unless-profile="lösungen"}
Bestimmen sie den Grenzwert (mit einer kurzen Begründung):
a) $\lim _{\substack{x\to2 \\ x<2}}\dfrac{-x}{x-2}$
b) $\lim _{\substack{x\to2 \\ x>-2}}\dfrac{x}{x+2}$
c) $\lim _{x\rightarrow \infty }1-e^{-x}$
d) $\lim _{x\rightarrow \infty }e^{-x^2}$
e) $\lim _{x\rightarrow \infty }\dfrac{x^3+2x}{x^4+5}$
f) $\lim _{x\rightarrow \infty }\dfrac{x^2-1}{x^2+1}$
g) $\lim _{x\rightarrow \infty }\dfrac{x^6}{e^x}$
h) $\lim _{x\rightarrow \infty }\dfrac{\sqrt{x}}{\ln \left( x\right) }$
:::

:::{.content-visible when-profile="lösungen"}
a) $ \displaystyle  \lim_{\substack{x\to 2 \\ x<2}} \ \frac{-x}{x-2} = \frac{-2}{-0} = +\infty $

b) $ \displaystyle \lim_{\substack{x\to -2 \\ x>-2}} \ \frac{x}{x+2} = \frac{-2}{+0} = -\infty $

c) $ \displaystyle \lim_{x\to \infty} \ 1-e^{-x}  =\lim_{x\to \infty} \ 1- \lim_{x\to \infty}\frac{1}{e^{x}}  = 1-\frac{1}{+\infty}=1-0  =1 $

d) $ \displaystyle \lim_{x\to \infty} \ e^{-x^2} =\displaystyle \lim_{x\to \infty} \ \frac{1}{e^{x^2}}=  \frac{1}{\infty}=0$

e) $\displaystyle \lim_{\substack{x\to \infty}} \ \frac{x^3+2x}{x^4+5} = \lim_{\substack{x\to \infty}} \ \frac{\frac{1}{x}+\frac{2}{x^3}} {1+\frac{5}{x^4}} =\frac{0+0}{1+0}=0$

f) $\displaystyle \lim_{\substack{x\to \infty}} \ \frac{x^2-1}{x^2+1} = \lim_{\substack{x\to \infty}} \ \frac{1-\frac{1}{x^2}} {1+\frac{1}{x^2}} =\frac{1+0}{1+0}=1$ 

g) $\displaystyle \lim_{x\to \infty} \ \frac{x^6}{e^x}  = 0$, da das exponentielles Wachstum im Nenner das Potenzwachstum im Zähler dominiert. 
$\displaystyle \lim_{x\to \infty} \ \frac{\sqrt{x}}{\ln(x)}=\infty$ da das Potenzwachstum im Zähler das logarithmische Wachstum im Nenner dominiert.
:::